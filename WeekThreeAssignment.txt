package training;

import java.util.*;

class TableCalculator {

	static void printTable(int n) {
		int x;
    for(int i=1;i<=10;i++) {
    	x=n*i;
    	System.out.println(x);
    }
	}

	public static void main(String[] args) {

		Scanner in = new Scanner(System.in);
        System.out.println("Enter the tables no:");
		int n = in.nextInt();

		printTable(n);

	}

}
------------------------------------------------------------------------

package training;
class Vehicle {
	public void noOfWheels() {
		System.out.println("No of wheels undefined");
	}
}
class scooter extends Vehicle {
	public void noOfWheels() {
		System.out.println("No of wheels two");
	}
}
class car extends Vehicle{
	public void noOfWheels() {
		System.out.println("No of wheels four");
	}
}
public class Main {

	public static void main(String[] args) {
		Vehicle vehicle = new Vehicle();  // Create a Vehicle object

		  Vehicle Scooter = new scooter();  // Create a Scooter object

		  Vehicle Car = new car();  // Create a Car object

		              vehicle.noOfWheels();     //call noOfWheels of vehicle class

		              Scooter.noOfWheels();   //call noOfWheels of Scooter class

		              Car.noOfWheels();         // call noOfWheels of Car class
	}

}
---------------------------------------------------------------------------------------

package training;

class ThreeDimensionShape

{

	double width, height, depth;

	// constructor used when all dimensions are defined

	ThreeDimensionShape(double w, double h, double d)

	{
		this.width=w;
		this.height=h;
		this.depth=d;

	}

// constructor used when one dimension is defined

	ThreeDimensionShape(double l)

	{
       this.width=l;
       this.height=l;
       this.depth=l;
	}

// constructor used when no dimension is specified

	ThreeDimensionShape()

	{
		this.width=0;
		this.height=0;
		this.depth=0;

	}

	// compute and return volume

	double volume()

	{

		return width * height * depth;

	}

}

// Driver code 

public class Test

{

	public static void main(String args[])

	{

		// create boxes using the various

		// constructors

		ThreeDimensionShape shape1 = new ThreeDimensionShape(5, 6, 7);

		ThreeDimensionShape shape2 = new ThreeDimensionShape();

		ThreeDimensionShape shape3 = new ThreeDimensionShape(8);

		double volume;

		// get volume of first box

		volume = shape1.volume();

		System.out.println(" Volume of shape1 is " + volume);

		// get volume of second box

		volume = shape2.volume();

		System.out.println(" Volume of shape2 is " + volume);

		// get volume of cube

		volume = shape3.volume();

		System.out.println(" Volume of shape3 is " + volume);

	}

}
-------------------------------------------------------------------------------------------------