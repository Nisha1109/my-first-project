package training;

import java.util.Scanner;

public class WeekTwoAssignment {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String[] tasks = new String[5];
		int i,j;
		System.out.println("ENTER FIVE TASKS:");
		for(i=0;i<5;i++) {
			tasks[i] = sc.nextLine();
		}
		
		
		//DELETE THE TASK
		System.out.println("Enter the task to be deleted");
		String deleted_task = sc.nextLine();
		for(i=0;i<5;i++) {
			
			if(tasks[i].compareTo(deleted_task)==0) {
				for(j=i+1;j<5;j++) {
					
					tasks[j-1]=tasks[j];
				}
				tasks[j-1]=" ";
				break;
			}
		}
		for(i=0;i<5;i++) 
			System.out.println("TASK " + (i+1) + " is :" + tasks[i]);
		
		//SEARCH THE TASK
		int flag=0;
		System.out.println("Enter the task to be searched");
		String searched_task = sc.nextLine();
		for(i=0;i<5;i++) {
			
			if(tasks[i].compareTo(searched_task)==0) {
				flag++;
			}
		}
		if(flag>0) {
			System.out.println("Element found");
		}
		else {
			System.out.println("Element not found");
		}
		//UPDATE THE TASK ARRAY
		System.out.println("Enter the task to be updated");
		String updated_task = sc.nextLine();
		System.out.println("Enter the indexes to be updated");
		int index_to_be_updated = sc.nextInt();
		tasks[index_to_be_updated-1]=updated_task;
		for(i=0;i<5;i++) 
			System.out.println("TASK " + (i+1) + " is :" + tasks[i]);
	
		sc.close();

	}

}
