package com.hcl.week1;
interface Common{
	abstract String markAttendance();
	abstract String dailyTask();
	abstract String displayDetails();
}
class Employee implements Common{
	public String markAttendance() {
		return "Attendance marked for today";
	}
	public String dailyTask() {
		return "complete coding of module1";
	}
	public String displayDetails() {
		return "employee details";
	}
	
}
class Manager implements Common{
	public String markAttendance() {
		return "Attendance marked for today";
	}
	public String dailyTask() {
		return "create project architecture";
	}
	public String displayDetails() {
		return "manager details";
	}
}


public class Day4Prog1 {
	public static void main(String[] args) {
		Common i1=new Employee();
		Common i2=new Manager();
		System.out.println(i1.markAttendance());
		System.out.println(i1.dailyTask());
		System.out.println(i1.displayDetails());
		System.out.println(i2.markAttendance());
		System.out.println(i2.dailyTask());
		System.out.println(i2.displayDetails());	
}
}

---------------------------------------------------------------------

package com.hcl.week1;

class Account{
	int accno;
	String custName;
	Account(int accno,String custName){
		this.accno=accno;
		this.custName=custName;
	}
	void display() {
		System.out.println(accno);
		System.out.println(custName);
	}
}
class currentAccount extends Account{
	int currBal;
	currentAccount(int accno,String custName,int currBal){
		super(accno,custName);
		this.currBal=currBal;
	}
	void display() {
		super.display();
		System.out.println(currBal);
	}
	
}
class accDetails extends currentAccount{
	int depositAmount,withdrawAmount;
	accDetails(int accno,String custName,int currBal,int depositAmount,int withdrawAmount){
		super(accno,custName,currBal);
		this.depositAmount=depositAmount;
		this.withdrawAmount=withdrawAmount;
	}
	void display() {
		super.display();
		System.out.println(depositAmount);
		System.out.println(withdrawAmount);
		
	}
	
}

------------------------------------------------------------------------------------------------